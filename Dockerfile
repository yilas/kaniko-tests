FROM debian:bullseye

# This is the anti-frontend. It never interacts with you  at  all,
# and  makes  the  default  answers  be used for all questions.
ENV DEBIAN_FRONTEND noninteractive

# Install packages
RUN apt-get update                               && \
    apt-get install      -yq apt-transport-https    \
                             ca-certificates        \
                             ssl-cert
COPY sources.list /etc/apt/sources.list
RUN apt-get update                                   && \
    apt-get dist-upgrade -yq                         && \
    apt-get install      -yq apt-transport-https        \
                             lftp \
                             binutils                   \
                             bsdmainutils               \
                             bzip2                      \
                             curl                       \
                             dfc                        \
                             dnsutils                   \
                             file                       \
                             htop                       \
                             iproute2                   \
                             iputils-ping               \
                             git                        \
                             gnupg                      \
                             jq                         \
                             lzop                       \
                             locales                    \
                             make                       \
                             most                       \
                             ncdu                       \
                             net-tools                  \
                             netcat-openbsd             \
                             openssl                    \
                             procps                     \
                             psmisc                     \
                             pv                         \
                             python3                    \
                             python3-pip                \
                             software-properties-common \
                             sudo                       \
                             tcputils                   \
                             tcpdump                    \
                             tree                       \
                             wget                       \
                             vim                        \
                             xz-utils                   \
                             zip                        \
                             zsh                     && \
    apt-get autoremove   -yq                         && \
    apt-get autoclean    -yq

# Install Docker repository (https://docs.docker.com/install/linux/docker-ce/debian)
RUN curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add - && \
    apt-key fingerprint 0EBFCD88                                                 && \
    add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian bullseye stable"

# Install Kubernetes repository
RUN curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add - && \
    echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" > /etc/apt/sources.list.d/kubernetes.list

# Locales
ENV LANG     en_US.UTF-8
ENV LANGUAGE en_US.UTF-8
ENV LC_ALL   en_US.UTF-8
RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && locale-gen

# Install RC files
RUN mkdir -p /root/.config/htop
ADD zshrc /root/.zshrc
ADD vimrc /root/.vimrc
#ADD vim /root/.vim
ADD htoprc /root/.config/htop/htoprc
